import { Component, MouseEventHandler } from "react"
import './Auth.css';

export interface IAuthComponentProps{
    login: string;
    password: string;    
}

export default class AuthComponent extends Component<{}, IAuthComponentProps>{

    constructor(props: IAuthComponentProps){
        super(props);
        this.state = { login: "", password: ""};
    }

    onLoginChanged = (e: any) => {
        this.setState({ login: e.target.value });
    };
    
    onPasswordChanged = (e: any) => {
        this.setState({ password: e.target.value });
    };


    login = ()=> {
        fetch('https://somewherepath.net/auth', {
            method: 'POST',
            headers: {
                Accept: "application/json, text/plain, */*",
                "Content-Type": "application/json",
              },
            body: JSON.stringify({
                login: this.state.login,
                password: this.state.password
            })
        })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          if (data.error) {
            alert("Error Password or Username");
          } else {
            window.open(
              "target.html"
            );
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    render() {
       return (
       <div>
        <table cellSpacing="10" cellPadding="0">
        <tr>
            <td>Login</td>
            <td><input type="text" onChange={this.onLoginChanged} /></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="text" onChange={this.onPasswordChanged} /></td>
        </tr>
        </table>
        <br></br>
        <button title="Login in" onClick={this.login}>Login in</button>
       </div> );
    }
}