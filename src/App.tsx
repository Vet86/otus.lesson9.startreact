import React from 'react';
import logo from './logo.svg';
import './App.css';
import AuthComponent from './Components/Auth/Auth';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <AuthComponent />
      </header>
    </div>
  );
}

export default App;
